from django import forms
from django.contrib import admin
from django.contrib.auth.models import User, Group
from django.urls import reverse
from django.utils.html import format_html

from archaeologist.admin import ArchaeologistAdmin
from excavation.models import Excavation, Layer, CulturalPeriod, StratigraphicRelationship


class LayerInline(admin.TabularInline):
    model = Layer
    extra = 1
    fields = ('depth', 'name', 'description', 'cultural_period', 'archaeologists', 'layers_above', 'layers_below')

    readonly_fields = ('depth', 'layers_above', 'layers_below')
    autocomplete_fields = ('archaeologists',)
    show_change_link = True

    def related_model_admin_link(self, obj):
        # Generate the URL for the related object's admin page
        url = reverse('admin:app_label_relatedmodel_change', args=[obj.id])
        # Return an HTML link to the related object's admin page
        return format_html('<a href="{}">View</a>', url)

    related_model_admin_link.short_description = 'Edit Layer'


class StratigraphicRelationshipForm(forms.ModelForm):
    class Meta:
        model = StratigraphicRelationship
        fields = '__all__'

    def clean(self):
        cleaned_data = super().clean()
        # Call the model's clean method
        print('CLEAN INSTANCE')
        self.instance.clean()
        return cleaned_data


class StratigraphicRelationshipAsTopInline(admin.TabularInline):
    model = StratigraphicRelationship
    fk_name = 'top_layer'
    extra = 1
    verbose_name_plural = 'Layers below'

    # form = StratigraphicRelationshipForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'bottom_layer':
            # Get the ID of the current layer
            layer_id = request.resolver_match.kwargs['object_id']
            # Get the excavation ID of the current layer
            excavation_id = Layer.objects.get(id=layer_id).excavation_id
            # Filter the layers based on the current excavation
            kwargs['queryset'] = Layer.objects.filter(excavation_id=excavation_id)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class StratigraphicRelationshipAsBottomInline(admin.TabularInline):
    model = StratigraphicRelationship
    fk_name = 'bottom_layer'
    verbose_name_plural = 'Layers above'
    extra = 1

    # form = StratigraphicRelationshipForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'top_layer':
            # Get the ID of the current layer
            layer_id = request.resolver_match.kwargs['object_id']
            # Get the excavation ID of the current layer
            excavation_id = Layer.objects.get(id=layer_id).excavation_id
            # Filter the layers based on the current excavation
            kwargs['queryset'] = Layer.objects.filter(excavation_id=excavation_id)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Excavation)
class ExcavationAdmin(admin.ModelAdmin):
    # List settings
    list_display = ('name', 'description', 'start_date', 'finished', 'location', 'created_at', 'updated_at')
    search_fields = ('name', 'description', 'location')
    sortable_by = ('name', 'description', 'start_date', 'finished', 'location', 'created_at', 'updated_at')
    ordering = ('-start_date',)
    date_hierarchy = 'start_date'
    list_filter = ('finished', 'location',)

    # Detail settings
    fields = (*list_display, 'archaeologists')
    readonly_fields = ('archaeologists', 'created_at', 'updated_at')
    inlines = (LayerInline,)


@admin.register(CulturalPeriod)
class CulturalPeriodAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'start_year', 'end_year', 'created_at', 'updated_at')
    search_fields = ('name', 'description')
    sortable_by = ('name', 'description', 'start_year', 'end_year', 'created_at', 'updated_at')
    ordering = ('-updated_at',)
    inlines = (LayerInline,)


@admin.register(Layer)
class LayerAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'excavation', 'cultural_period', 'layers_above', 'layers_below',
                    'created_at', 'updated_at')
    search_fields = ('name', 'description', 'excavation__name')
    readonly_fields = ('layers_above', 'layers_below', 'depth')
    sortable_by = ('name', 'description', 'excavation', 'cultural_period', 'depth', 'created_at', 'updated_at')
    ordering = ('-updated_at',)
    date_hierarchy = 'excavation__start_date'
    list_filter = ('excavation__location',)
    autocomplete_fields = ('archaeologists',)
    inlines = (StratigraphicRelationshipAsTopInline, StratigraphicRelationshipAsBottomInline)


admin.site.unregister(User)
admin.site.unregister(Group)
