from datetime import datetime
from enum import Enum

from itertools import groupby
from operator import attrgetter

from django.core.exceptions import ValidationError
from django.db import models

from archaeologist.admin import ArchaeologistAdmin
from archaeologist.models import Archaeologist


class Excavation(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    start_date = models.DateField(default=datetime.now)
    location = models.CharField(max_length=100)
    finished = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def archaeologists(self) -> str:
        names = {a.name for layer in self.layers.all().prefetch_related('archaeologists') for a in
                 layer.archaeologists.all()}
        return f'{len(names)}: ' + ', '.join(sorted(names))

    def __str__(self):
        return self.name


class CulturalPeriod(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    start_year = models.IntegerField()
    end_year = models.IntegerField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Cultural periods'
        ordering = ('-start_year', '-end_year')

    def __str__(self):
        return self.name


class Layer(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    cultural_period = models.ForeignKey(CulturalPeriod, on_delete=models.PROTECT)
    excavation = models.ForeignKey(Excavation, on_delete=models.CASCADE, related_name='layers')
    archaeologists = models.ManyToManyField(Archaeologist, related_name='layers', blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.excavation.name} - {self.name}'

    @property
    def layers_above(self) -> str:
        return ', '.join([sr.top_layer.name for sr in self.stratigraphic_relationships_as_bottom.all()])

    @property
    def layers_below(self) -> str:
        return ', '.join([sr.bottom_layer.name for sr in self.stratigraphic_relationships_as_top.all()])

    @property
    def depth(self) -> int:
        if self.layers_above:
            return max([sr.top_layer.depth for sr in self.stratigraphic_relationships_as_bottom.all()]) + 1
        else:
            return 0

    @property
    def admin_link(self) -> str:
        return f'<a href="/admin/excavation/layer/{self.id}/change/">{self.name}</a>'


class StratigraphicRelationship(models.Model):
    top_layer = models.ForeignKey(Layer, on_delete=models.CASCADE,
                                  related_name='stratigraphic_relationships_as_top')
    bottom_layer = models.ForeignKey(Layer, on_delete=models.CASCADE,
                                     related_name='stratigraphic_relationships_as_bottom')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def clean(self):
        # Validate that the top layer is not the same as the bottom layer
        if self.top_layer == self.bottom_layer:
            raise ValidationError('The top layer cannot be the same as the bottom layer')
        # Validate that the layers cannot have a circular dependency
        if self.top_layer.depth > self.bottom_layer.depth:
            raise ValidationError('The top layer cannot be deeper than the bottom layer')


class ComponentType(Enum):
    GEOLOGICAL = 'geological'
    ORGANIC = 'organic'
    ARTIFICIAL = 'artificial'


class Component(models.Model):
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=100, choices=[(tag, tag.value) for tag in ComponentType])

    def __str__(self):
        return self.name


class StratigraphicUnit(models.Model):
    intervention = models.CharField(max_length=100, name='intervenció')
    sector = models.CharField(max_length=100, name='sector')
    zone = models.CharField(max_length=100, name='zona')
    inlet = models.CharField(max_length=100, name='cala')
    range = models.CharField(max_length=100, name='àmbit')

    description = models.TextField(name='descripció')
    preservation_state = models.TextField(name='estat de conservació')
    excavation_process = models.TextField(name="Proces d'excavació")
    date_of_action = models.DateField(name="data d'actuació")

    responsible_archaeologist = models.ForeignKey(Archaeologist, on_delete=models.PROTECT,
                                                  name='arqueòleg responsable', related_name='units')
    revised_by = models.ForeignKey(Archaeologist, on_delete=models.PROTECT,
                                   name='revisada per', related_name='revised_units')

    components = models.ManyToManyField(Component, related_name='stratigraphic_units')

    # seqüència física
    DEFAULTS = dict(on_delete=models.SET_NULL, null=True, blank=True)
    equal_to = models.OneToOneField('self', name='igual a', **DEFAULTS)
    in_contact_with = models.OneToOneField('self', name='en contacte amb', **DEFAULTS)
    attached_to = models.OneToOneField('self', name='se li adossa', **DEFAULTS)
    adjacent_to = models.OneToOneField('self', name="s'adossa a", **DEFAULTS)
    covers = models.OneToOneField('self', related_name='covered_by', name='cobert per', **DEFAULTS)
    cuts = models.OneToOneField('self', related_name='cut_by', name='talla', **DEFAULTS)
    refills = models.OneToOneField('self', related_name='refilled_by', name='reomple', **DEFAULTS)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


# Add a property called `involved_in` to class Archaeologist with the names of
# each layer in which the archaeologist has worked.
def worked_on(archaeologist) -> str:
    result = ''
    layers_worked_on = archaeologist.layers.all().select_related('excavation'). \
        order_by('excavation__start_date', 'excavation__name')
    for excavation, layers in groupby(layers_worked_on, attrgetter('excavation')):
        result += (f'{excavation.name}: ' +
                   ', '.join([layer.name for layer in layers]) + '\n')
    return result

    # return f'{len(names)} layers: ' + ', '.join(sorted(names))


Archaeologist.add_to_class('worked_on', worked_on)
ArchaeologistAdmin.readonly_fields = (*ArchaeologistAdmin.readonly_fields, 'worked_on')
