import random
from datetime import datetime, timedelta
from typing import List

from django.core.management import BaseCommand

from archaeologist.models import Archaeologist
from common.utils import pascal_to_spaces
from excavation.models import Excavation, Layer, CulturalPeriod, StratigraphicRelationship

location_names = ['Barcelona', 'Girona', 'Lleida', 'Tarragona', 'Reus', 'Sabadell', 'Terrassa']
excavation_names = [
    'Montjuïc Excavation', 'Barcino Dig', 'Tarragona Terrace Project', 'Besòs River Expedition',
    'Llobregat Lakeside Survey', 'Gothic Quarter Exploration', 'Sitges Seaside Excavation',
    'Eixample Extension Project', 'Montserrat Mountain Expedition', 'Sant Cugat Citadel Survey'
]
cultural_periods = [
    {'name': 'Prehistoric Catalonia',
     'description': 'The period before written records.',
     'start_year': -10000, 'end_year': -2500},
    {'name': 'Neolithic Era',
     'description': 'The period characterized by the development of agriculture and the use of polished stone tools.',
     'start_year': -2500, 'end_year': -2200},
    {'name': 'Chalcolithic Period',
     'description': 'The period marked by the use of both stone and copper tools.',
     'start_year': -2200, 'end_year': -1800},
    {'name': 'Bronze Age',
     'description': 'The period characterized by the use of bronze tools and weapons.',
     'start_year': -1800, 'end_year': -900},
    {'name': 'Iron Age',
     'description': 'The period marked by the widespread use of iron tools and the emergence of complex societies.',
     'start_year': -900, 'end_year': -500},
    {'name': 'Iberian Period',
     'description': 'The period when the Iberians inhabited the region.',
     'start_year': -500, 'end_year': -218},
    {'name': 'Roman Period',
     'description': 'The period of Roman rule in Catalonia.',
     'start_year': -218, 'end_year': 409},
    {'name': 'Visigothic Period',
     'description': 'The period of Visigothic rule following the fall of the Western Roman Empire.',
     'start_year': 409, 'end_year': 711},
    {'name': 'Islamic Period', 'description': 'The period of Islamic rule in Catalonia.',
     'start_year': 711, 'end_year': 801},
    {'name': 'County of Barcelona',
     'description': 'The period of the establishment of the County of Barcelona.',
     'start_year': 801, 'end_year': 1162},
    {'name': 'Catalan-Aragonese Confederation',
     'description': 'The period of the union between the Crown of Aragon and the Crown of Catalonia.',
     'start_year': 1162, 'end_year': 1410},
    {'name': 'Catalan Revolt',
     'description': 'The period of the Catalan Revolt against the rule of Philip IV of France.',
     'start_year': 1640, 'end_year': 1659},
    {'name': 'War of Spanish Succession',
     'description': 'The period of the War of Spanish Succession, which ended with the loss of Catalan liberties.',
     'start_year': 1701, 'end_year': 1714},
    {'name': 'Industrial Revolution',
     'description': 'The period of industrialization and urbanization in Catalonia.',
     'start_year': 1760, 'end_year': 1860},
    {'name': 'Second Spanish Republic',
     'description': 'The period of the Second Spanish Republic, '
                    'characterized by political instability and social reforms.',
     'start_year': 1931, 'end_year': 1939},
    {'name': 'Spanish Civil War',
     'description': 'The period of the Spanish Civil War, during which Catalonia played a significant role.',
     'start_year': 1936, 'end_year': 1939},
    {'name': 'Francoist Spain',
     'description': 'The period of dictatorship under Francisco Franco.',
     'start_year': 1939, 'end_year': 1975},
    {'name': 'Transition to Democracy',
     'description': 'The period of transition from dictatorship to democracy in Spain.',
     'start_year': 1975, 'end_year': 1982},
    {'name': 'Modern Catalonia',
     'description': 'The contemporary period of Catalonia characterized by autonomy within Spain and cultural revival.',
     'start_year': 1982, 'end_year': 2023},
]

layer_descriptions = iter([
    ('AncientPottery', 'Layer containing numerous fragments of ancient pottery.'),
    ('StructuralRemains', 'Stratum with visible remains of ancient buildings and structures.'),
    ('AshDeposit', 'Deposit of ash and charcoal indicating past fire activity.'),
    ('FossilizedPlants', 'Layer with embedded fossilized remains of ancient plants.'),
    ('MedievalArtifacts', 'Level containing artifacts of daily life from medieval times.'),
    ('RomanOccupation', 'Stratum with evidence of Roman occupation, including coins and pottery.'),
    ('AncientRoad', 'Layer suggesting the presence of an ancient road or pathway.'),
    ('CoastalHabitation', 'Deposit of marine shells indicating ancient coastal habitation.'),
    ('PrehistoricAnimals', 'Stratum containing bones and remains of prehistoric animals.'),
    ('OrganicMaterial', 'Layer rich in organic material preserving ancient plant specimens.'),
    ('VolcanicAsh', 'Deposit of volcanic ash from an ancient eruption.'),
    ('FossilizedFish', 'Sedimentary layer with fossilized remains of ancient fish.'),
    ('BronzeAgeArtifacts', 'Level containing artifacts indicative of Bronze Age metalworking.'),
    ('HumanFootprints', 'Stratum with well-preserved footprints of ancient humans.'),
    ('AlluvialDeposit', 'Deposit of alluvial soil indicating ancient river activity.'),
    ('TextileProduction', 'Layer containing remnants of ancient textile production.'),
    ('PollenGrains', 'Sediment layer with embedded pollen grains from ancient flora.'),
    ('CeramicArtifacts', 'Level with well-preserved ceramics and pottery.'),
    ('AgriculturalActivity', 'Stratum containing traces of ancient agricultural practices.'),
    ('LimestoneFragments', 'Deposit of limestone fragments from ancient geological events.'),
    ('MolluskShells', 'Layer rich in fossilized mollusk shells.'),
    ('TradeNetworks', 'Sedimentary strata with evidence of ancient trade networks.'),
    ('IberianCivilization', 'Level containing artifacts of ancient Iberian civilization.'),
    ('HotSpringActivity', 'Deposit of tufa indicating ancient hot spring activity.'),
    ('NeolithicBurials', 'Stratum with evidence of Neolithic burial practices.'),
    ('DefensiveFortifications', 'Layer containing remnants of ancient defensive fortifications.'),
    ('FossilizedTrees', 'Sediment deposit with fossilized tree stumps.'),
    ('MoorishArtifacts', 'Level rich in artifacts of Moorish culture.'),
    ('VolcanicPumice', 'Deposit of volcanic pumice from ancient eruptions.'),
    ('HuntingActivity', 'Stratum with evidence of prehistoric hunting activity.'),
    ('MarineFossils', 'Layer rich in marine fossils from ancient seabeds.'),
    ('PlantFossils', 'Sedimentary deposit with well-preserved fossilized plant specimens.'),
    ('RitualSacrifices', 'Level containing remnants of ancient ritual sacrifices.'),
    ('LimestoneConglomerate', 'Deposit of limestone conglomerate from ancient geological events.'),
    ('IronAgeArtifacts', 'Stratum with evidence of Iron Age artifacts and metalworking.'),
    ('AgriculturalTerraces', 'Layer containing remnants of ancient agricultural terraces.'),
    ('MarineReptileRemains', 'Sediment deposit with fossilized marine reptile remains.'),
    ('PotteryProduction', 'Level rich in artifacts of ancient pottery production.'),
    ('VolcanicBreccia', 'Deposit of volcanic breccia from ancient volcanic eruptions.'),
    ('BurialMounds', 'Stratum with evidence of ancient burial mounds.'),
    ('UrbanInfrastructure', 'Layer containing remnants of ancient urban infrastructure.'),
    ('LakeEcology', 'Sedimentary deposit with evidence of ancient lake ecology.'),
    ('RomanFrescoes', 'Level with well-preserved Roman frescoes.'),
    ('LimestoneKarst', 'Deposit of limestone karst formations.'),
    ('MetalSmelting', 'Stratum with evidence of ancient metal smelting.'),
    ('PlantSeeds', 'Layer rich in fossilized plant seeds.'),
    ('GlacialActivity', 'Sediment deposit with evidence of ancient glacial activity.'),
    ('MaritimeTrade', 'Level containing artifacts of ancient maritime trade.'),
    ('VolcanicLapilli', 'Deposit of volcanic lapilli from ancient volcanic eruptions.'),
    ('Woodworking', 'Stratum with evidence of ancient woodworking activities.'),
    ('MiningOperations', 'Layer containing remnants of ancient mining operations.'),
    ('AmmoniteShells', 'Sedimentary deposit with fossilized ammonite shells.'),
    ('TextileTrade', 'Level rich in artifacts of ancient textile trade.'),
    ('LimestoneTravertine', 'Deposit of limestone travertine from ancient springs.'),
    ('PotteryKilns', 'Stratum with evidence of ancient pottery kilns.'),
    ('CaveBearFossils', 'Layer containing remnants of ancient cave bear fossils.'),
    ('CoralReefs', 'Sediment deposit with evidence of ancient coral reefs.'),
    ('PhoenicianArtifacts', 'Level with well-preserved Phoenician artifacts.'),
    ('PyroclasticFlows', 'Deposit of volcanic ash from ancient pyroclastic flows.'),
    ('Toolmaking', 'Stratum with evidence of ancient toolmaking activities.'),
    ('MarineInvertebrates', 'Layer rich in fossilized marine invertebrates.'),
    ('FloodEvents', 'Sedimentary deposit with evidence of ancient flood events.'),
    ('ReligiousCeremonies', 'Level containing artifacts of ancient religious ceremonies.'),
    ('TravertineFormations', 'Deposit of limestone travertine formations from ancient springs.'),
    ('PotteryKilns', 'Stratum with evidence of ancient pottery kilns.'),
    ('CaveBearFossils', 'Layer containing remnants of ancient cave bear fossils.'),
    ('CoralReefs', 'Sediment deposit with evidence of ancient coral reefs.'),
    ('PhoenicianArtifacts', 'Level with well-preserved Phoenician artifacts.'),
    ('PyroclasticFlows', 'Deposit of volcanic ash from ancient pyroclastic flows.'),
    ('Toolmaking', 'Stratum with evidence of ancient toolmaking activities.'),
    ('MarineInvertebrates', 'Layer rich in fossilized marine invertebrates.'),
    ('FloodEvents', 'Sedimentary deposit with evidence of ancient flood events.'),
    ('ReligiousCeremonies', 'Level containing artifacts of ancient religious ceremonies.'),
    ('TravertineFormations', 'Deposit of limestone travertine formations from ancient springs.'),
    ('MetallicOres', 'Stratum with evidence of ancient metallic ores and mining activities.'),
    ('ShellMiddens', 'Layer containing remnants of ancient shell middens left by human activity.'),
    ('AqueductRemains', 'Level with visible remains of ancient aqueducts and water channels.'),
    ('CavePaintings', 'Stratum with well-preserved ancient cave paintings and rock art.'),
    ('SedimentaryDeposits', 'Deposit of sedimentary layers indicating past geological events.'),
    ('MilitaryFortifications', 'Layer containing remnants of ancient military fortifications.'),
    ('SculptedFigures', 'Stratum with well-preserved sculpted figures and statues.'),
    ('MarineSediments', 'Sedimentary layer with evidence of ancient marine sediments.'),
    ('TradeGoods', 'Level containing artifacts indicative of ancient trade goods and commodities.'),
    ('SandstoneFormations', 'Deposit of sandstone formations from ancient geological processes.'),
    ('CisternSystems', 'Stratum with visible remains of ancient cisterns and water storage systems.'),
    ('Petroglyphs', 'Layer containing well-preserved ancient petroglyphs and rock engravings.'),
    ('GeologicalFaults', 'Deposit indicating ancient geological fault lines and fractures.'),
    ('MudbrickStructures', 'Stratum with remnants of ancient mudbrick structures and buildings.'),
    ('FlintTools', 'Layer containing artifacts of ancient flint tool manufacturing.'),
    ('GlacialMoraines', 'Deposit of glacial moraines from ancient glacial activity.'),
    ('TanneryRemains', 'Stratum with visible remains of ancient tanneries and leatherworking.'),
    ('SharkTeeth', 'Layer rich in fossilized shark teeth from ancient marine environments.'),
    ('ClayPits', 'Deposit of clay pits indicating past clay extraction activities.'),
    ('TerracottaFigurines', 'Stratum with well-preserved terracotta figurines and artifacts.'),
    ('LithicScatters', 'Layer containing scattered lithic artifacts from ancient human activity.'),
    ('SaltDeposits', 'Deposit of salt indicating ancient salt extraction and processing.'),
    ('PalaeolithicHabitations', 'Stratum with evidence of Palaeolithic human habitation sites.'),
    ('TufaFormations', 'Layer containing tufa formations from ancient mineral-rich springs.'),
    ('ShipwreckRemains', 'Deposit with remnants of ancient shipwrecks and maritime artifacts.'),
    ('BronzeAgeTools', 'Layer containing well-preserved bronze tools from the Bronze Age period.'),
    ('CopperSmelting', 'Stratum with evidence of ancient copper smelting and metallurgical activities.'),
    ('GrainStoragePits', 'Deposit with remnants of ancient grain storage pits and granaries.'),
    ('VolcanicAsh', 'Layer containing volcanic ash deposits from ancient volcanic eruptions.'),
    ('BakeryRemains', 'Stratum with visible remains of ancient bakeries and bread-making facilities.'),
    ('FossilizedPlantMaterial', 'Deposit rich in fossilized plant material from ancient ecosystems.'),
    ('ObsidianArtifacts', 'Layer containing artifacts made from obsidian, a volcanic glass.'),
    ('CoralReefs', 'Stratum with evidence of ancient coral reefs and marine ecosystems.'),
    ('PollenSamples', 'Deposit with preserved pollen samples providing insights into ancient vegetation.'),
    ('TarPits', 'Layer containing remnants of ancient tar pits used by prehistoric humans.'),
    ('GlasswareFragments', 'Stratum with fragments of ancient glassware and glass artifacts.'),
    ('IronSmeltingFurnaces', 'Deposit with remnants of ancient iron smelting furnaces.'),
    ('AmphoraFragments', 'Layer containing fragments of ancient amphorae used for storage.'),
    ('QuartzCrystals', 'Stratum with deposits of quartz crystals from ancient geological formations.'),
    ('TextileWorkshops', 'Deposit with visible remains of ancient textile workshops and looms.'),
    ('WoodCharcoal', 'Layer containing well-preserved wood charcoal from ancient fires.'),
    ('PetrochemicalResidues', 'Stratum with residues of ancient petrochemical substances.'),
    ('OrnamentalJewelry', 'Deposit containing ornamental jewelry and adornments from antiquity.'),
    ('TerraCottaTiles', 'Layer with remnants of ancient terra cotta tiles used in construction.'),
    ('LeadIngots', 'Stratum with evidence of ancient lead ingots and metalworking.'),
    ('RomanRoads', 'Deposit with remnants of ancient Roman roads and infrastructure.'),
    ('AmmoniteFossils', 'Layer rich in fossilized ammonite shells from ancient seas.'),
    ('PotteryKilns', 'Stratum with remnants of ancient pottery kilns and ceramics production.'),
    ('DiatomaceousEarth', 'Deposit containing diatomaceous earth from ancient lake beds.'),
    ('SilverCoins', 'Layer with ancient silver coins and numismatic artifacts.'),
    ('ClayBrickStructures', 'Stratum with remnants of ancient clay brick structures.'),
    ('TombArtifacts', 'Deposit with artifacts and grave goods from ancient tombs.'),
    ('MegalithicMonuments', 'Layer with evidence of ancient megalithic monuments and structures.'),
    ('PapyrusFragments', 'Stratum with fragments of ancient papyrus scrolls and documents.'),
    ('OlivePresses', 'Deposit with remains of ancient olive presses and oil production facilities.'),
    ('GraniteBlocks', 'Layer containing large granite blocks quarried in antiquity.'),
    ('SaltwaterIntrusion', 'Stratum with evidence of ancient saltwater intrusion into freshwater aquifers.'),
    ('LinenTextiles', 'Deposit containing well-preserved linen textiles from ancient times.'),
    ('AqueductArchitectures', 'Layer with remnants of ancient aqueduct architectures and engineering.'),
    ('CoinMintingWorkshops', 'Stratum with evidence of ancient coin minting workshops.'),
    ('MarbleQuarries', 'Deposit with remnants of ancient marble quarries and extraction sites.'),
    ('CaveBearFossils', 'Layer rich in fossilized remains of ancient cave bears.'),
    ('TinAlloys', 'Stratum with evidence of ancient tin alloys and metalworking.'),
    ('LeadPipes', 'Deposit containing remnants of ancient lead pipes and plumbing.'),
    ('PumiceStones', 'Layer with deposits of pumice stones from ancient volcanic eruptions.'),
    ('CeramicFigurines', 'Stratum with well-preserved ceramic figurines and statuettes.'),
    ('TufaLimestone', 'Deposit with tufa limestone formations from ancient geological processes.'),
    ('Graeco-RomanPottery', 'Layer containing pottery and ceramics from the Graeco-Roman period.'),
    ('IronAgeWeapons', 'Stratum with artifacts of ancient Iron Age weapons and armaments.'),
    ('PrehistoricRockShelters', 'Deposit with evidence of prehistoric rock shelters and dwellings.'),
    ('LavaFlows', 'Layer with evidence of ancient lava flows and volcanic activity.'),
    ('LeadSeals', 'Stratum containing ancient lead seals used for authentication and sealing documents.'),
    ('ObsidianBlades', 'Deposit with artifacts of ancient obsidian blades and tools.'),
    ('CoralFragments', 'Layer with fragments of ancient coral skeletons.'),
    ('TanninPits', 'Stratum with remnants of ancient tannin pits used in leather production.'),
    ('BrickKilns', 'Deposit containing remnants of ancient brick kilns and ceramics firing.'),
    ('GoldNuggets', 'Layer with ancient gold nuggets and alluvial gold deposits.'),
    ('AmphoraFragments', 'Layer containing fragments of ancient amphorae used for storage and transport.'),
    ('CopperSmelting', 'Stratum with evidence of ancient copper smelting and metallurgical activity.'),
    ('CharcoalRemains', 'Deposit of charcoal remains from ancient fires and hearths.'),
    ('GraniteQuarries', 'Layer containing evidence of ancient granite quarries and stone extraction.'),
    ('MarbleColumns', 'Stratum with remnants of ancient marble columns and architectural elements.'),
    ('RomanRoads', 'Layer with visible remains of ancient Roman roads and pathways.'),
    ('PollenSamples', 'Deposit containing well-preserved pollen samples for paleoenvironmental analysis.'),
    ('GlassBeads', 'Stratum with artifacts of ancient glass beads and jewelry.'),
    ('AgriculturalTerraces', 'Layer with evidence of ancient agricultural terraces and farming practices.'),
    ('ShipTimbers', 'Deposit with well-preserved timbers from ancient ship construction.'),
    ('TombChambers', 'Stratum containing remnants of ancient tomb chambers and burial sites.'),
    ('CoralReefs', 'Layer with evidence of ancient coral reefs and marine ecosystems.'),
    ('BronzeFigures', 'Deposit containing bronze figurines and sculptures from antiquity.'),
    ('AdobeStructures', 'Stratum with remnants of ancient adobe structures and dwellings.'),
    ('VolcanicAsh', 'Layer containing deposits of ancient volcanic ash and tephra.'),
    ('BoneTools', 'Stratum with artifacts of ancient bone tools and implements.'),
    ('BrickKilns', 'Deposit with remnants of ancient brick kilns and pottery workshops.'),
    ('ObsidianSources', 'Layer containing evidence of ancient obsidian sources and tool production.'),
    ('GypsumDeposits', 'Stratum with deposits of ancient gypsum and mineral formations.'),
    ('LapidariumFragments', 'Deposit containing fragments of ancient lapidarium sculptures.'),
    ('WoodCharcoal', 'Stratum with well-preserved wood charcoal from ancient fires.'),
    ('AlabasterVessels', 'Layer with remnants of ancient alabaster vessels and containers.'),
    ('IronSmelting', 'Deposit with evidence of ancient iron smelting and forging activities.'),
    ('PhoenicianArtifacts', 'Stratum containing artifacts of ancient Phoenician origin.'),
    ('ShellfishMiddens', 'Layer with remnants of ancient shellfish middens and culinary refuse.'),
    ('CopperOreVeins', 'Stratum containing veins of ancient copper ore deposits.'),
    ('FossilizedFootprints', 'Deposit with fossilized footprints from ancient human or animal activity.'),
    ('BasaltColumns', 'Layer with remnants of ancient basalt columns and geological formations.'),
    ('FrescoFragments', 'Stratum containing fragments of ancient frescoes and wall paintings.'),
    ('CobblestonePavements', 'Deposit with remnants of ancient cobblestone pavements and roads.'),
    ('StalactiteFormations', 'Layer with formations of ancient stalactites and speleothems.'),
    ('LeadIngots', 'Stratum containing artifacts of ancient lead ingots and metalwork.'),
    ('BronzeAgePottery', 'Deposit with fragments of pottery from the Bronze Age period.'),
    ('PapyrusScrolls', 'Layer containing remnants of ancient papyrus scrolls and manuscripts.'),
    ('GypsumCrystals', 'Stratum with well-preserved gypsum crystals from ancient mineral deposits.'),
    ('GoldNuggets', 'Deposit with remnants of ancient gold nuggets and placer deposits.'),
    ('GeologicalFossils', 'Layer containing fossils of ancient geological specimens.'),
    ('SilverCoins', 'Stratum with artifacts of ancient silver coins and currency.'),
    ('TuffFormations', 'Deposit with formations of ancient tuff and volcanic rock.'),
    ('StuccoReliefs', 'Layer containing remnants of ancient stucco reliefs and decorative motifs.'),
    ('PetrochemicalResidues', 'Stratum with residues of ancient petrochemical substances.'),
    ('PollenAnalysis', 'Deposit with samples for pollen analysis to reconstruct past environments.'),
    ('MaritimeHarbors', 'Layer with evidence of ancient maritime harbors and port facilities.'),
    ('LithicToolkits', 'Stratum with artifacts of ancient lithic toolkits and implements.'),
    ('CeramicFragments', 'Deposit with fragments of ancient ceramic vessels and pottery.'),
    ('MarlDeposits', 'Layer containing deposits of ancient marl and sedimentary rock.'),
    ('SilkRoadTrade', 'Stratum with evidence of ancient Silk Road trade routes and commerce.'),
    ('MegalithicMonuments', 'Deposit with remnants of ancient megalithic monuments and structures.'),
    ('BambooRafts', 'Layer with evidence of ancient bamboo rafts and watercraft.'),
    ('PollenGrains', 'Stratum with well-preserved pollen grains for environmental reconstruction.'),
]
)


def generate_periods() -> List[CulturalPeriod]:
    """
    Generate fake cultural periods
    """

    result = []
    for period in cultural_periods:
        result.append(CulturalPeriod.objects.create(**period))
    return result


def generate_excavation(name: str, periods: List[CulturalPeriod]):
    """
    Generate a fake excavation
    """

    location = random.choice(location_names)
    days_ago = random.randint(1, 365)
    start_date = datetime.now() - timedelta(days=days_ago)
    finished = random.randint(200, 400) < days_ago
    excavation = Excavation.objects.create(
        name=name,
        description=f'This is the description of the {name}',
        start_date=start_date,
        location=location,
        finished=finished
    )

    last_layer = None
    for layer_index in range(1, 11):
        name, description = next(layer_descriptions)
        name = pascal_to_spaces(name)
        period = random.choice(periods)
        layer = Layer(
            name=name,
            description=description,

            cultural_period=period,
            excavation=excavation
        )
        layer.save()
        nr_archaeologists = random.randint(1, 3)
        for _ in range(nr_archaeologists):
            layer.archaeologists.add(random.choice(Archaeologist.objects.all()))
        if last_layer:
            StratigraphicRelationship.objects.create(top_layer=last_layer, bottom_layer=layer)

        # Set the last layer to the current layer in 2/3 of the cases
        if not last_layer or random.choice([True, True, False]):
            last_layer = layer


class Command(BaseCommand):
    help = 'Create or update some default list of locations in the database'

    def handle(self, *args, **options):
        periods = generate_periods()
        self.stdout.write(self.style.SUCCESS(f'Successfully created {len(periods)} cultural periods'))
        for excavation_name in excavation_names:
            generate_excavation(excavation_name, periods)
        self.stdout.write(self.style.SUCCESS(f'Successfully created {len(excavation_names)} excavations'))
