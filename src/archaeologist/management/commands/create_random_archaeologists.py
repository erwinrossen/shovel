import random

from django.core.management import BaseCommand

from archaeologist.models import Archaeologist

female_first_names = ['Nuria', 'Montserrat', 'Laia', 'Aina', 'Eva', 'Anna', 'Elena', 'Marta', 'Clara', 'Judit']
male_first_names = ['Jordi', 'Lluis', 'Pau', 'Joan', 'Carles', 'Marc', 'Sergi', 'Pol', 'Oriol', 'Marti']
last_names = [
    'Martínez', 'Sánchez', 'García', 'Fernández', 'López', 'Pérez', 'González', 'Rodríguez', 'Martín', 'Serra',
    'Solà', 'Vidal', 'Costa', 'Puig', 'Balaguer', 'Molina', 'Miró', 'Parés', 'Casanova', 'Barcelona',
    'Miquel', 'Riera', 'Navarro', 'Oliver', 'Soler', 'Garrido', 'Giménez', 'Serra', 'Pons', 'Vives',
    'Gràcia', 'Font', 'Guerrero', 'Ferrer', 'Cortés', 'Sala', 'Gisbert', 'Quintana', 'Lladó', 'Badia',
    'Folch', 'Torró', 'Borràs', 'Berenguer', 'Fábregas', 'Gil', 'Andreu', 'Gallego', 'Vila', 'Gaya'
]
specializations = [
    'Prehistoric Archaeology', 'Roman Archaeology', 'Medieval Archaeology', 'Industrial Archaeology',
    'Underwater Archaeology', 'Landscape Archaeology', 'Experimental Archaeology', 'Bioarchaeology',
    'Ceramic Analysis', 'Archaeological Conservation'
]
domains = ['gmail.com', 'hotmail.com', 'yahoo.com', 'outlook.com']


def generate_archaeologist() -> Archaeologist:
    """
    Generate a fake archaeologist
    """

    gender = random.choice(['M', 'F'])
    if gender == 'F':
        first_name = random.choice(female_first_names)
    else:
        first_name = random.choice(male_first_names)
    last_name = random.choice(last_names)

    domain = random.choice(domains)
    email = f'{first_name}.{last_name}@{domain}'
    email = email.lower().replace(' ', '')
    specialization = random.choice(specializations)
    bio = (f'Hi, I am {first_name} {last_name}. I am an archaeologist based in Catalonia, '
           f'specializing in {specialization}.')
    return Archaeologist(gender=gender, first_name=first_name, last_name=last_name, email=email, bio=bio)


class Command(BaseCommand):
    help = 'Create or update some default list of locations in the database'

    def handle(self, *args, **options):
        archaeologists = [generate_archaeologist() for _ in range(20)]
        existing_emails_archaeologists = set(Archaeologist.objects.values_list('email', flat=True))
        new_archaeologists = [archaeologist for archaeologist in archaeologists
                              if archaeologist.email not in existing_emails_archaeologists]
        Archaeologist.objects.bulk_create(new_archaeologists)
        self.stdout.write(self.style.SUCCESS(f'Successfully created {len(new_archaeologists)} archaeologists'))
