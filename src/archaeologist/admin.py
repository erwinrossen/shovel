from django.contrib import admin

from archaeologist.models import Archaeologist


@admin.register(Archaeologist)
class ArchaeologistAdmin(admin.ModelAdmin):
    list_display = ('email', 'first_name', 'last_name', 'gender')
    search_fields = ('email', 'first_name', 'last_name')
    sortable_by = ('email', 'first_name', 'last_name', 'gender')
    ordering = ('email',)
