from django.apps import AppConfig


class ArchaeologistConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'archaeologist'
