import re
import timeit


# Methods to list

def pascal_to_list(input_string: str) -> list[str]:
    """
    Convert PascalCase to a list of lowercased words

    >>> pascal_to_list('PascalCase')
    ['pascal', 'case']
    >>> pascal_to_list('AnotherExample')
    ['another', 'example']
    >>> pascal_to_list('ABC')
    ['a', 'b', 'c']

    Implementation notes:
    - [A-Z]: This part of the regex is a character class that matches any uppercase letter from 'A' to 'Z'.
        It matches a single uppercase letter.
    - [^A-Z]*: This part of the regex matches zero or more occurrences of any character except uppercase letters.
        The ^ inside the square brackets negates the character class, so [^A-Z] matches any character that is not an
        uppercase letter. The * quantifier means that it will match zero or more occurrences of such characters.
    - [A-Z][^A-Z]*: This regex pattern is the combination of the two parts described above. It matches an uppercase
        letter followed by zero or more non-uppercase letters.
    - re.findall(pattern, string): This function searches for all occurrences of the specified pattern in the given
        string and returns them as a list of strings.
    """

    # Split the string at each uppercase letter
    split = re.findall(r'[A-Z][^A-Z]*', input_string)

    # Convert each word to lowercase
    return [word.lower() for word in split]


def snake_to_list(input_string: str) -> list[str]:
    """
    Convert snake_case to a list of lowercased words

    >>> snake_to_list('snake_case')
    ['snake', 'case']
    >>> snake_to_list('another_example')
    ['another', 'example']
    >>> snake_to_list('abc')
    ['a', 'b', 'c']
    """

    # Split the string at each underscore
    split = input_string.split('_')

    # Convert each word to lowercase
    return [word.lower() for word in split]


def camel_to_list(input_string: str) -> list[str]:
    """
    Convert camelCase to a list of lowercased words

    >>> camel_to_list('camelCase')
    ['camel', 'case']
    >>> camel_to_list('anotherExample')
    ['another', 'example']
    >>> camel_to_list('ABC')
    ['a', 'b', 'c']
    """

    # Split the string at each uppercase letter
    split = re.findall(r'[A-Z][^A-Z]*', input_string)

    # Convert each word to lowercase
    return [word.lower() for word in split]


def spaces_to_list(input_string: str) -> list[str]:
    """
    Convert a string with spaces between words to a list of words

    >>> spaces_to_list('Pascal Case')
    ['pascal', 'case']
    >>> spaces_to_list('Another Example')
    ['another', 'example']
    >>> spaces_to_list('A B C')
    ['a', 'b', 'c']
    """

    # Split the string at each space
    return input_string.split(' ')


# Methods from list

def list_to_pascal(input_list: list[str]) -> str:
    """
    Convert a list of words to PascalCase

    >>> list_to_pascal(['pascal', 'case'])
    'PascalCase'
    >>> list_to_pascal(['another', 'example'])
    'AnotherExample'
    >>> list_to_pascal(['a', 'b', 'c'])
    'ABC'
    """

    # Convert the first letter of each word to uppercase and join the words
    return ''.join(word.capitalize() for word in input_list)


def list_to_snake(input_list: list[str]) -> str:
    """
    Convert a list of words to snake_case

    >>> list_to_snake(['snake', 'case'])
    'snake_case'
    >>> list_to_snake(['another', 'example'])
    'another_example'
    >>> list_to_snake(['a', 'b', 'c'])
    'a_b_c'
    """

    # Join the words with underscores
    return '_'.join(input_list)


def list_to_camel(input_list: list[str]) -> str:
    """
    Convert a list of words to camelCase

    >>> list_to_camel(['camel', 'case'])
    'camelCase'
    >>> list_to_camel(['another', 'example'])
    'anotherExample'
    >>> list_to_camel(['a', 'b', 'c'])
    'aBC'
    >>> list_to_camel([])
    ''
    """

    if not input_list:
        return ''

    # Convert the first letter of each word to uppercase, except for the first word
    return input_list[0] + ''.join(word.capitalize() for word in input_list[1:])


def list_to_spaces(input_list: list[str]) -> str:
    """
    Convert a list of words to a string with spaces between the words

    >>> list_to_spaces(['pascal', 'case'])
    'pascal case'
    >>> list_to_spaces(['another', 'example'])
    'another example'
    >>> list_to_spaces(['a', 'b', 'c'])
    'a b c'
    """

    # Join the words with spaces
    return ' '.join(input_list)


# Methods using from and to list

def pascal_to_snake(input_string: str) -> str:
    return list_to_snake(pascal_to_list(input_string))


def pascal_to_camel(input_string: str) -> str:
    return list_to_camel(pascal_to_list(input_string))


def pascal_to_spaces(input_string: str, use_regex: bool = True) -> str:
    """
    Convert PascalCase to the same string with spaces between the words

    >>> pascal_to_spaces('PascalCase')
    'Pascal Case'
    >>> pascal_to_spaces('AnotherExample')
    'Another Example'
    >>> pascal_to_spaces('ABC')
    'A B C'

    Implementation notes:
    - (?<!^): This negative lookbehind assertion ensures that the character is not preceded by the start of the string.
    - (?=[A-Z]): This positive lookahead assertion ensures that the current character is followed by an uppercase
                 character. We use a lookahead assertion instead of directly matching the uppercase letter itself
                 because we want to insert spaces between words without consuming the uppercase letters. This ensures
                 that each uppercase letter remains part of the word it belongs to, and only the spaces between words
                 are inserted.
    """

    if use_regex:
        return re.sub(r'(?<!^)(?=[A-Z])', ' ', input_string)
    else:
        return list_to_spaces(pascal_to_list(input_string))


def snake_to_pascal(input_string: str) -> str:
    return list_to_pascal(snake_to_list(input_string))


def snake_to_camel(input_string: str) -> str:
    return list_to_camel(snake_to_list(input_string))


def snake_to_spaces(input_string: str) -> str:
    return list_to_spaces(snake_to_list(input_string))


def camel_to_pascal(input_string: str) -> str:
    return list_to_pascal(camel_to_list(input_string))


def camel_to_snake(input_string: str) -> str:
    return list_to_snake(camel_to_list(input_string))


def camel_to_spaces(input_string: str) -> str:
    return list_to_spaces(camel_to_list(input_string))


def spaces_to_pascal(input_string: str) -> str:
    return list_to_pascal(spaces_to_list(input_string))


def spaces_to_snake(input_string: str) -> str:
    return list_to_snake(spaces_to_list(input_string))


def spaces_to_camel(input_string: str) -> str:
    return list_to_camel(spaces_to_list(input_string))


def method1():
    pascal_to_spaces('PascalCase', use_regex=True)


def method2():
    pascal_to_spaces('PascalCase', use_regex=False)


if __name__ == '__main__':
    # Set up the timeit module with appropriate parameters
    number_of_iterations = 100_000  # Adjust this based on the complexity of your methods
    repeat_count = 5  # Number of times to repeat the measurement for accuracy

    # Measure the execution times for method 1
    time_method1 = [
        timeit.timeit(method1, number=number_of_iterations, globals=globals())
        for _ in range(repeat_count)
    ]

    # Measure the execution times for method 2
    time_method2 = [
        timeit.timeit(method2, number=number_of_iterations, globals=globals())
        for _ in range(repeat_count)
    ]

    # Calculate the average time for each method
    avg_time_method1 = sum(time_method1) / repeat_count
    avg_time_method2 = sum(time_method2) / repeat_count

    # Print the results
    print(f"Average execution time for Method 1: {avg_time_method1:.6f} seconds")
    print(f"Average execution time for Method 2: {avg_time_method2:.6f} seconds")

    # Compare the results
    time_difference = (avg_time_method1 - avg_time_method2) / avg_time_method1 * 100
    if time_difference < -10:
        print(f'Method 1 is faster by {abs(time_difference)}%')
    elif time_difference > 10:
        print(f'Method 2 is faster by {abs(time_difference)}%')
    else:
        print(f'Both methods have similar execution times, difference is {abs(time_difference):.0f}%')
