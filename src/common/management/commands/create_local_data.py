import random

from django.contrib.auth.models import User
from django.core.management import BaseCommand, call_command

random.seed(684)


class Command(BaseCommand):
    def handle(self, *args, **options):
        call_command('migrate')
        if not User.objects.filter(is_staff=True).exists():
            User.objects.create_superuser(username='admin', email='admin@shovel.es', password='admin')
        call_command('create_random_archaeologists')
        call_command('create_random_excavations')
