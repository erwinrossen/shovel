# SHOVEL

SHOVEL (System for Handling Observations, Visuals, and Excavation Logs) is a tool for managing and analyzing 
archaeological data. It is designed to be a flexible and extensible system that can be used to manage data from a wide 
variety of archaeological projects. SHOVEL is designed to be used by archaeologists, and is designed to be easy to use 
and understand.
